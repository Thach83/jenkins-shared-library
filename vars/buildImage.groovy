#!/urs/bin/env groovy

def call() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t tominhthach/minhthach:v1 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push tominhthach/minhthach:v1'
    }
}
